package com.andrius.covid19app.utils;

import java.util.Scanner;

/**
 * Code created by Andrius on 2020-08-23
 */
public class IOservice {

    private static Scanner scanner = new Scanner(System.in);

    public static void printWelcomeMessage() {
        String separator = "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";
        System.out.printf("%s%nCovid-19 statistics app powered by Covid19API.com%n%s%n", separator, separator);
    }

    public static void message(String message) {
        System.out.println(message);
    }

    public static int getUserInputNumber() {
        int intValue;
        try {
            intValue = Integer.parseInt(scanner.next());
        } catch (NumberFormatException e) {
            System.out.println("Input is not a number. Try again!");
            return getUserInputNumber();
        }
        return intValue;
    }

    public static String parseMonthsDaysToString(int number) {
        if (number < 10) {
            return String.format("0%d", number);
        } else {
            return String.valueOf(number);
        }
    }

    public static String getSearchKey() {
        message("\nEnter at least 3 letters to find a country:");
        String searchTerm = scanner.next();
        if (searchTerm.length() < 3) {
            return getSearchKey();
        } else {
            return searchTerm;
        }
    }
}
