package com.andrius.covid19app.utils;

/**
 * Code created by Andrius on 2020-08-23
 */
public class MonthsDaysInputs {

    public static int getMonth() {
        int number = IOservice.getUserInputNumber();
        if (number > 0 && number < 13) {
            return number;
        } else {
            IOservice.message("Only numbers from 1 to 12 allowed. Enter again.");
            return getMonth();
        }
    }

    public static int getDay(int month) {
        int number = IOservice.getUserInputNumber();
        if (month == 2) {
            if (number > 0 && number < 29) {
                return number;
            } else {
                IOservice.message("Only numbers from 1 to 28 allowed. Enter again.");
                return getDay(month);
            }
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (number > 0 && number < 31) {
                return number;
            } else {
                IOservice.message("Only numbers from 1 to 30 allowed. Enter again.");
                return getDay(month);
            }
        } else {
            if (number > 0 && number < 32) {
                return number;
            } else {
                IOservice.message("Only numbers from 1 to 31 allowed. Enter again.");
                return getDay(month);
            }
        }
    }
}
