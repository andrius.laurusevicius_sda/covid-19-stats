package com.andrius.covid19app.task;

import com.andrius.covid19app.model.Covid19;
import com.andrius.covid19app.service.DbReader;
import com.andrius.covid19app.utils.IOservice;

/**
 * Code created by Andrius on 2020-08-23
 */
public class Controller {

    Covid19 virus = new DbReader().create();
    Search search = new Search();

    public void runApp() {
        IOservice.printWelcomeMessage();
        printGlobalStatistics();
        while (true) {
            printMenu();
            selectOption();
        }
    }

    private void printMenu() {
        IOservice.message("\nSelect from menu by entering the number:" +
                "\n1 - Show country with highest number of total cases" +
                "\n2 - Find country stats by keyword" +
                "\n3 - Top infected countries by total cases" +
                "\n4 - Country statistics from selected period" +
                "\n0 - Exit program");
    }

    public void printGlobalStatistics() {
        System.out.println(virus.getGlobal());
    }

    private void printCountrySearchResult() {
        search.printCountryInformationByKey(virus.getSortedCountries());
    }

    public void selectOption() {
        int answer = IOservice.getUserInputNumber();
        switch (answer) {
            case 0:
                shutDown();
                break;
            case 1:
                search.getCountryWithMaxTotalCases(virus.getSortedCountries());
                break;
            case 2:
                printCountrySearchResult();
                break;
            case 3:
                search.printTopInfectedCountries(virus.getSortedCountries());
                break;
            case 4:
                new CountryStatistics().printCountryStats(virus.getSlugByCountryName());
                break;
            default:
                IOservice.message("Wrong input, try again!");
        }
    }

    private void shutDown() {
        System.exit(0);
    }

}
