package com.andrius.covid19app.task;

import com.andrius.covid19app.model.CountryStatsHistory;
import com.andrius.covid19app.service.LiveReader;

/**
 * Code created by Andrius on 2020-08-24
 */
public class CountryStatistics {

    public void printCountryStats(String slug) {
        CountryStatsHistory[] stats = getCountryStatsHistory(slug);
        System.out.println("\nSelected country: " + stats[0].getCountryName());
        for (CountryStatsHistory item : stats
        ) {
            System.out.println(item);
        }
        printPercentageInCaseChange(stats[0].getCases(), stats[stats.length - 1].getCases());
    }

    private CountryStatsHistory[] getCountryStatsHistory(String slug) {
        return new LiveReader().create(slug);
    }

    private void printPercentageInCaseChange(int a, int b) {
        if (a == 0) {
            a = 1;
        }
        double change = (((double) b - a) / a) * 100;
        System.out.printf("Total change in this period: %.0f%%%n", change);
    }
}
