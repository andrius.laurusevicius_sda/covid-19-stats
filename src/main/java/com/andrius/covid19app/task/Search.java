package com.andrius.covid19app.task;

import com.andrius.covid19app.model.Country;
import com.andrius.covid19app.utils.IOservice;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Code created by Andrius on 2020-08-23
 */
public class Search {

    public void printCountryInformationByKey(List<Country> countries) {
        String key = IOservice.getSearchKey();
        for (Country value : countries
        ) {
            if (value.getCountryName().toLowerCase().contains(key.toLowerCase())) {
                System.out.println(value);
            }
        }
    }

    public void getCountryWithMaxTotalCases(List<Country> countries) {
        System.out.println(countries.get(0));
    }

    public void printTopInfectedCountries(List<Country> countries) {
        IOservice.message("How many countries you wanna to show, enter number:");
        int numberOfCountriesToPrint = IOservice.getUserInputNumber();
        AtomicInteger x = new AtomicInteger();
        countries.stream().
                limit(numberOfCountriesToPrint).
                forEach(country -> System.out.println((x.getAndIncrement() + 1) + ") " +
                        country.totalCountryCasesInformation() + "\n"));
    }
}
