package com.andrius.covid19app.service;

import com.andrius.covid19app.model.Covid19;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Code created by Andrius on 2020-08-23
 */
public class DbReader {

    private static final String COVID_URL = "https://api.covid19api.com/summary";

    Gson gson;

    public DbReader () {
        gson = new GsonBuilder().create();
    }

    public Covid19 create() {
        return gson.fromJson(new HttpRequestUrl().getHttpResponse(COVID_URL), Covid19.class);
    }
}
