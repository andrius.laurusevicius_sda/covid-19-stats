package com.andrius.covid19app.service;

import com.andrius.covid19app.model.CountryStatsHistory;
import com.andrius.covid19app.utils.IOservice;
import com.andrius.covid19app.utils.MonthsDaysInputs;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Code created by Andrius on 2020-08-23
 */
public class LiveReader {

    private static final String CONST_PATH_START = "https://api.covid19api.com/total/country/";
    private static final String CONST_PATH_PART = "status/confirmed?from=";

    private String getUrlPathByCountrySlug(String slug) {
        return String.format("%s%s/%s%s", CONST_PATH_START, slug, CONST_PATH_PART, getDateForUrlPath());
    }

    private String getDateForUrlPath() {
        IOservice.message("Enter start month");
        int montFrom = MonthsDaysInputs.getMonth();
        IOservice.message("Enter start day");
        int dayFrom = MonthsDaysInputs.getDay(montFrom);
        IOservice.message("Enter end month");
        int montTo = MonthsDaysInputs.getMonth();
        IOservice.message("Enter end day");
        int dayTo = MonthsDaysInputs.getDay(montTo);

        return String.format("2020-%s-%sT00:00:00Z&to=2020-%s-%sT00:00:00Z",
                IOservice.parseMonthsDaysToString(montFrom),
                IOservice.parseMonthsDaysToString(dayFrom),
                IOservice.parseMonthsDaysToString(montTo),
                IOservice.parseMonthsDaysToString(dayTo));
    }

    public CountryStatsHistory[] create(String slug) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(new HttpRequestUrl().getHttpResponse(getUrlPathByCountrySlug(slug)), CountryStatsHistory[].class);
    }
}
