package com.andrius.covid19app.model;

import com.andrius.covid19app.utils.IOservice;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Code created by Andrius on 2020-08-23
 */
public class Covid19 {

    @SerializedName("Global")
    private Global global;

    @SerializedName("Countries")
    private List<Country> countries;

    public List<Country> getCountries() {
        return countries;
    }

    public List<Country> getSortedCountries() {
        return this.countries.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    public Global getGlobal() {
        return global;
    }

    public String getSlugByCountryName() {
        String keyword = IOservice.getSearchKey();
        return getSortedCountries().stream()
                .filter(country -> country
                        .getCountryName().toLowerCase()
                        .contains(keyword))
                .map(Country::getSlug)
                .findFirst()
                .orElse("Not found");
    }
}
