package com.andrius.covid19app.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Code created by Andrius on 2020-08-23
 */
public class Country implements Comparable<Country> {

    @SerializedName("Country")
    private String countryName;

    @SerializedName("CountryCode")
    private String countryCode;

    @SerializedName("Slug")
    private String slug;

    @SerializedName("NewConfirmed")
    private int newConfirmed;

    @SerializedName("TotalConfirmed")
    private int totalConfirmed;

    @SerializedName("NewRecovered")
    private int newRecovered;

    @SerializedName("TotalRecovered")
    private int totalRecovered;

    @SerializedName("Date")
    private Date date;

    public String getCountryName() {
        return countryName;
    }


    public String getSlug() {
        return slug;
    }

    public String getDate() {
        return new SimpleDateFormat("kk:mm dd-MM-yyyy").format(this.date);
    }

    public String totalCountryCasesInformation() {
        return String.format("%s has %d total confirmed cases." +
                "%nLatest update - %s", this.countryName, this.totalConfirmed, getDate());
    }

    public String getTopCountryStats() {
        return String.format("%nCountry with highest number of total cases:%n%s, with total %d cases (updated %s).",
                this.countryName, this.totalConfirmed, getDate());
    }

    @Override
    public String toString() {
        return String.format("%s has %d new cases and %d total confirmed cases." +
                "%nLatest update - %s", this.countryName, this.newConfirmed, this.totalConfirmed, getDate());
    }

    @Override
    public int compareTo(Country country) {
        return Integer.compare(this.totalConfirmed, country.totalConfirmed);
    }
}
