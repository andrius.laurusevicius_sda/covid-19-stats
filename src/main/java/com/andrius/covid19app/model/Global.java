package com.andrius.covid19app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Code created by Andrius on 2020-08-23
 */
public class Global {

    @SerializedName("NewConfirmed")
    private int newConfirmed;

    @SerializedName("TotalConfirmed")
    private int totalConfirmed;

    @SerializedName("TotalRecovered")
    private int totalRecovered;

    @Override
    public String toString() {
        return String.format("Latest global update:%n" +
                "New confirmed cases today - %d%nTotal confirmed cases - %d%n" +
                "Total recovered - %d", this.newConfirmed, this.totalConfirmed, this.totalRecovered);
    }

}
