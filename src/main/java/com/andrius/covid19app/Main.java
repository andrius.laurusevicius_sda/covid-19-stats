package com.andrius.covid19app;

import com.andrius.covid19app.task.Controller;

/**
 * Code created by Andrius on 2020-08-23
 */
public class Main {
    public static void main(String[] args) {
        new Controller().runApp();
    }
}
