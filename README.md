## Covid-19 Statistics App (powered by Covid19api.com API)

### Content

[1. How to run program](#how-to-run-this-programm)

[2. Description](#description)

[3. Keywords](#keywords)

### How to run this program:

To run this program you will need to clone the project _[git clone https://gitlab.com/andrius.laurusevicius_sda/covid-19-stats-using.git]_. Open this project in your IDE and run Main (Main.java) class.

### Description

This simple Covid-19 statistics app is using Covid19api.com API to get the latest information and change history of each country.

The app makes request to https://api.covid19api.com/summary to retrieve main Covid-19 stats off all countries.

**1)** First option prints the country, with the highest number of total cases: 

![alt text](https://i.ibb.co/HqGhkFc/top-country-stats2.png)

**2)** Second option - country search by keyword or full name, which prints main statistics:

![alt text](https://i.ibb.co/cxn4frn/country-by-keyword2.png)

**3)** The third option shows countries ordered by the biggest number of cases. You can select how many countries to show:

![alt text](https://i.ibb.co/GvSkk6q/top-countries-by-total-cases2.png)

**4)** The fourth option shows statistics history for selected country and selected period of time. Additionally change percentage is calculated and displayed. It makes a live request using this URL format https://api.covid19api.com/total/country/south-africa/status/confirmed?from=2020-03-01T00:00:00Z&to=2020-04-01T00:00:00Z

![alt text](https://i.ibb.co/bsrPkys/country-stats-history2.png)

### Keywords:

Collections API, Gson, @SerializedName, Comparable, Comparator, Lambda expressions, Maven project, Exceptions, Composition.